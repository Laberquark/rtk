#pragma once

#include <exception>
#include <string>

#include "defines/core.h"

class OpenGLException : public std::runtime_error
{
public:
    OpenGLException(GLenum error) : std::runtime_error("OpenGL error: " + std::to_string(error)) {
    }

    OpenGLException(GLenum error, std::string message) : std::runtime_error(message + " (OpenGL error " + std::to_string(error) + ")") {}

    static void check(const char * message = nullptr)
    {
        GLenum error = glGetError();
        if(error != GL_NO_ERROR)
        {
            throw OpenGLException(error, message);
        }
    }
};
