#infdef RTK_GL_TYPES_H
#define RTK_GL_TYPES_H

#include "../glinclude.h"

#ifndef __gl_h_

extern "C"
{
    typedef unsigned int GLenum;
}

#endif // __gl_h_
#endif // RTK_GL_TYPES_H
