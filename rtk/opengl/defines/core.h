#ifndef RTK_GL_CORE_H
#define RTK_GL_CORE_H

#include "../glinclude.h"

#ifndef __gl_h_

#include "../types.h"

extern "C"
{

    GLenum glGetError(void);

}

#endif // __gl_h_
#endif // RTK_GL_CORE_H
