#pragma once

#include <exception>

#include "Texture.h"
#include "../OpenGLException.h"
#include "../../tools/id/IDCounter.h"
#include "../../tools/Context.h"

typedef IDProvider<GLint> TextureUnitProvider;

#include <iostream>

class TextureFactory : public Context<TextureFactory>
{
public:
    ~TextureFactory()
    {
        delete internalTextureUnitProvider;
        internalTextureUnitProvider = nullptr;
    }

    SimpleTexture createTexture()
    {
        assertInitialized();
        return SimpleTexture().create(_textureUnitProvider->next());
    }

    SimpleTexture& createTexture(SimpleTexture& texture)
    {
        assertInitialized();
        return texture.create(_textureUnitProvider->next());
    }

    void initialize(TextureUnitProvider* textureUnitProvider = nullptr)
    {
        if(_initialized)
            throw std::logic_error("Texture factory already initialized");

        if(!textureUnitProvider)
        {
            GLint maxId;
            glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &maxId);
            OpenGLException::check();

            internalTextureUnitProvider = new IDCounter<GLint>(0, maxId);
            textureUnitProvider = internalTextureUnitProvider;
        }
        this->_textureUnitProvider = textureUnitProvider;

        _initialized = true;
    }

    inline void assertInitialized()
    {
        if(!_initialized)
            throw std::logic_error("The texture factory has not been initialized");
    }

    TextureUnitProvider* textureUnitProvider() { return _textureUnitProvider; }

private:
    TextureUnitProvider* _textureUnitProvider = nullptr;
    TextureUnitProvider* internalTextureUnitProvider = nullptr;
    bool _initialized = false;
};
