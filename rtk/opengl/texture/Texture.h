#pragma once

#include <exception>

#include "../glinclude.h"

class Texture
{
public:
    static const int InvalidUnitIndex = -1;
    static const GLuint InvaliTextureId = static_cast<GLuint>(-1);

    Texture(GLuint textureId, int unitIndex, GLenum textureType = GL_TEXTURE_2D) :
        textureId(textureId), unitIndex(unitIndex), textureType(textureType) {}

    GLuint getId() const { return textureId; }

    int getUnitIndex() const { return unitIndex; }

protected:
    GLuint textureId;
    int unitIndex = InvalidUnitIndex;
    const GLenum textureType;
};

class SimpleTexture : public Texture
{
public:
    SimpleTexture(GLenum textureType = GL_TEXTURE_2D) : Texture(Texture::InvaliTextureId, Texture::InvalidUnitIndex, textureType)
    {
    }

    SimpleTexture& create(int unitIndex)
    {
        if(this->unitIndex != Texture::InvalidUnitIndex)
            throw std::logic_error("Texture already created");
        this->unitIndex = unitIndex;
        glActiveTexture(GL_TEXTURE0 + unitIndex);
        glGenTextures(1, &textureId);
        return *this;
    }

    void use()
    {
        activate();
        bind();
    }

    void activate() const
    {
        checkCreated();
        glActiveTexture(GL_TEXTURE0 + unitIndex);
    }

    void bind() const
    {
        checkCreated();
        glBindTexture(textureType, textureId);
    }

    inline bool isCreated() const { return unitIndex != Texture::InvalidUnitIndex; }

    inline void checkCreated() const
    {
        if(!isCreated())
            throw std::logic_error("Texture needs to be created first before being used");
    }
};
