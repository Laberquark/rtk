#pragma once

#include <stdexcept>

#include "../glinclude.h"

class ShaderProgram
{
public:
    constexpr static const GLuint InvalidShaderID = 0;

    ShaderProgram(GLuint programID = InvalidShaderID) : programID(programID)
    {
    }

    GLuint operator()() const { return programID; }

    void use()
    {
        if(programID == InvalidShaderID)
            throw std::logic_error("Invalid shader program ID");
        glUseProgram(programID);
    }
private:
    GLuint programID;
};
