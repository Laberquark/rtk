#pragma once

template<typename Type>
class Singleton
{
public:
    template<typename...Args>
    static Type* instance(Args...args)
    {
        if(!_instance)
            _instance = new Type(args...);
        return _instance;
    }
protected:
    Singleton(){}
private:
    constexpr static Type* _instance = nullptr;
};
