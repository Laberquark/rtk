#pragma once

#include <exception>
#include <atomic>
#include <limits>

#include "IDProvider.h"

template<typename Type>
class AbstractIDCounter : public IDProvider<Type>
{
public:
    AbstractIDCounter(Type startValue, Type limit) : counter(startValue), limitValue(limit)
    {
    }

    void setLimit(Type limit)
    {
        this->limitValue = limit;
    }

    Type limit() const { return limitValue; }

protected:
    Type limitValue;
    std::atomic<Type> counter;
};

template<typename Type>
class IDCounter : public AbstractIDCounter<Type>
{
public:
    IDCounter(Type startValue = 0, Type maxValue = std::numeric_limits<Type>::max()) : AbstractIDCounter(startValue, maxValue) {}

    Type next() override
    {
        if(counter > limitValue)
            throw IDLimitReached(limitValue);
        return counter++;
    }
};

template<typename Type>
class InvertedIDCounter : public AbstractIDCounter<Type>
{
public:
    InvertedIDCounter(Type startValue = std::numeric_limits<Type>::max(), Type minValue = 0) : AbstractIDCounter(startValue, minValue) {}

    Type next() override
    {
        if(counter < limitValue)
            throw IDLimitReached(limitValue);
        return counter--;
    }
};

template<typename Type>
class IDLimitReached : public std::overflow_error
{
public:
    IDLimitReached(Type limit) : std::overflow_error(std::string("The ID limit of ") + std::to_string(limit) + " was reached")
    {
    }
};
