#pragma once

template<typename Type>
class IDProvider
{
public:
    virtual Type next() = 0;
};
