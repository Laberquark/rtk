#pragma once

#include <list>

class Event
{
};

template<typename EventType, typename...ParameterTypes>
class EventHandler
{
public:
    virtual void handleEvent(EventType& event, ParameterTypes...args) = 0;
};

template<typename EventType, typename...ParameterTypes>
class EventDispatcher
{
public:
    void addEventHandler(EventHandler<EventType, ParameterTypes...>& eventHandler)
    {
        handlers.push_back(&eventHandler);
    }

protected:
    void dispatchEvent(EventType& event, ParameterTypes...args)
    {
        for(auto& handler : handlers)
        {
            handler->handleEvent(event, args...);
        }
    }
private:
    std::list<EventHandler<EventType, ParameterTypes...>*> handlers;
};
