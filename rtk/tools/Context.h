#pragma once

#include <list>

template<typename ContextType>
class Context
{
public:
    template<typename ContextType, typename...T>
    static ContextType* create(T...args, bool setCurrent = true)
    {
        ContextType* newContext = new ContextType(args...);
        if(setCurrent)
            newContext->setCurrent();
        return newContext;
    }

    static ContextType* current()
    {
        return currentContext;
    }

    static void setCurrent(ContextType* context)
    {
        currentContext = context;
    }

    void setCurrent()
    {
        setCurrent(static_cast<ContextType*>(this));
    }
private:
    inline static ContextType* currentContext = nullptr;
};

template<typename ContextType>
class ContextGroup
{
public:
    template<typename ContextType, typename...T>
    static ContextType* emplace(T...args, bool setCurrent = true)
    {
        ContextType* newContext = new ContextType(args...);
        if(setCurrent)
            newContext->setCurrent();
        contextGroup.emplace_back(newContext);
        return newContext;
    }

    ~ContextGroup()
    {
        for(ContextType* context : contextGroup)
        {
            delete context;
        }
        contextGroup.clear();
    }
private:
    inline static std::list<ContextType*> contextGroup;
};
