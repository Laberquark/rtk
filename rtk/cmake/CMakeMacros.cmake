# Common macro to signal an incomplete configuration.
# The resulting behaviour can be adapted to the desired application, for example QMake acts different to CMake-Gui.
macro(inclompete_configuration msg)
	#Sending the error and setting the INCOMPLETE flag so the process can be aborted after all user configuration values are known is a good approach:
	set(INCOMPLETE true)
        #message(SEND_ERROR "${msg}")
	
	#In Qt SEND_ERROR leads to an invalid code model, but warnings are not reported properly, only as general message in the output
	message(WARNING "${msg}")
endmacro()

macro(check_path path msg)
	if(NOT EXISTS "${path}")
		inclompete_configuration("${msg} (path: ${path})")
	endif()
endmacro()

macro(check_path_variable variable msg)
	if(NOT ${variable})
		inclompete_configuration("Path variable ${variable} not defined")
	else()
		check_path(${${variable}} ${msg})
	endif()
endmacro()

macro(add_include item)
	check_path(${${item}} "Include directory not found")
	include_directories(${${item}})
endmacro()

macro(add_link_directory item)
	check_path(${${item}} "Library directory not found")
	link_directories(${${item}})
endmacro()

macro(add_lib item)
	list(APPEND LIBS ${item})
endmacro()

macro(output_file file)
    add_custom_command(TARGET ${CMAKE_PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different
        ${file}
        $<TARGET_FILE_DIR:${CMAKE_PROJECT_NAME}>)
endmacro()

macro(lib_target_name output_variable release_lib debug_lib)
    if(${CMAKE_BUILD_TYPE} MATCHES Debug)
        set(${output_variable} ${debug_lib})
    else()
        set(${output_variable} ${release_lib})
    endif()
endmacro()

# Fetches all source files (.c, .cpp, .cxx, .cc) in the given path and it's sub directories and appends them to the given variable
macro(fetch_source_files path output_variable)
    file(GLOB_RECURSE fetched_file_list RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ${path}/*.cpp ${path}/*.c ${path}/*.cxx ${path}/*.cc)
    list(APPEND ${output_variable} ${fetched_file_list})
endmacro()